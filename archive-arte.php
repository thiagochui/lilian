<?php get_header(); ?>

<?php $category = get_queried_object(); ?>

<div id="artes">
	<div class="container">
		<?php $categorias = get_terms('artes', array("parent" => 0, "hide_empty" => false)); ?>
		<?php foreach ($categorias as $cat): ?>
		<h2>
			<?php echo $cat->name; ?> /
			<?php foreach (get_terms('artes', array("parent" => $cat->term_id, "hide_empty" => false)) as $sub): ?>
				<?php echo $sub->name; ?>
		</h2>
			<?php endforeach ?>	
		<?php endforeach ?>

		<?php
		$args = array(
			'posts_per_page'   => 9999,
			'post_type'        => 'arte',
			'post_status'      => 'publish',
			'offset'		   => 0
		);
		$artes = get_posts( $args );

		if ( have_posts() ) : while ( have_posts() ) : the_post();

		foreach($artes as $arte): ?>
			<div class="col-xs-12 col-sm-6 col-md-4 conteudo-archive-arte">
				<?php $img = get_field('imagem_destaque_arte'); ?>
				<a href="<?php the_permalink(); ?>"><img src="<?php echo $img['sizes']['large']; ?>"></a>
				<h5><?php echo $arte->post_title; ?></h5>
				<?php the_field('conteudo_arte_archive'); ?>
			</div>

            <?php endforeach; ?>
		<?php endwhile; endif; ?>
	</div>
</div>


<?php
get_footer(); ?>