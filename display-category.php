<?php $category = get_queried_object(); ?>
<?php $categorias = get_terms('artes', array("parent" => 0, "hide_empty" => false)); ?>
<?php foreach ($categorias as $cat): ?>	 
<a data-fadeout  href="<?php echo get_term_link( $cat->slug, "artes" ); ?>">
<?php echo $cat->name; ?>
</a>
<?php foreach (get_terms('artes', array("parent" => $cat->term_id, "hide_empty" => false)) as $sub): ?>
<a data-fadeout  href="<?php echo get_term_link( $sub->slug, "artes" ); ?>">
	<?php echo $sub->name; ?>
</a>
<?php endforeach ?>	
<?php endforeach ?>