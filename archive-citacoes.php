<?php get_header(); ?>

<div id="citacoes">
	<div class="container">
		<h2><?php _e('<!--:pb-->Citações<!--:--><!--:en-->Quotes<!--:--><!--:es-->Citas<!--:-->'); ?></h2>

		<div class="col-xs-12 col-sm-6 col-md-10 col-md-offset-2">
		<?php
			$args = array(
				'posts_per_page'   => -1,
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'citacoes',
				'tax_query' => array(
					array(
						'taxonomy' => 'cat_citacoes',
						'field'    => 'slug',
						'terms'    => 'destaque',
					)
				)
			);
			$citacoes = get_posts( $args );
			foreach($citacoes as $citacao) { ?>
			<div class="box-citacoes box-destaque">
				<h5><?php echo $citacao->post_title; ?></h5>
				<?php echo the_field('conteudo_citacao', $citacao->ID); ?>
				<?php $nota = get_field('notas_explicativas', $citacao->ID); ?>
				<?php if($nota): ?>
					<div class="nota-explicativa"><?php echo the_field('notas_explicativas', $citacao->ID); ?></div>
				<?php endif; ?>
				<h4><?php echo get_post_meta($citacao->ID, 'referencia_citacoes', true); ?></h4>
			</div>
			<?php }  ?>
		</div>
		
		<div class="img-citacoes col-xs-12">
			<?php $imagem = get_field('imagem_principal', 9); ?>
			<img class="img-lilian" src="<?php echo $imagem['sizes']['large']; ?>">
			<span class="fotografo-citacoes">© Eduardo Nascimento</span> 
		</div>
		<script type="text/javascript">
			jQuery( document ).ready(function() {
				var altura = jQuery(".img-citacoes img").height() + 20;
				jQuery(".img-citacoes").css("height", altura);
			});
		</script>
		
		<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-2">
		<?php
			$args = array(
				'posts_per_page'   => -1,
				'orderby'          => 'date',
				'order'            => 'DESC',
				'post_type'        => 'citacoes',
				'tax_query' => array(
					array(
						'taxonomy' => 'cat_citacoes',
						'field'    => 'slug',
						'terms'    => 'normal',
					)
				)
			);
			$citacoes = get_posts( $args );
			foreach($citacoes as $citacao) { ?>
			<div class="box-citacoes">
				<h5><?php echo $citacao->post_title; ?></h5>
				<?php echo the_field('conteudo_citacao', $citacao->ID); ?>
				<?php $nota = get_field('notas_explicativas', $citacao->ID); ?>
				<?php if($nota): ?>
					<div class="nota-explicativa"><?php echo the_field('notas_explicativas', $citacao->ID); ?></div>
				<?php endif; ?>
				<h4><?php echo get_post_meta($citacao->ID, 'referencia_citacoes', true); ?></h4>
			</div>
			<?php }  ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>