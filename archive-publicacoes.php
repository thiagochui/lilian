<?php get_header(); ?>

<div id="page-publicacoes">
	<div class="content-size">
		<h2><?php _e('<!--:pb-->Publicações<!--:--><!--:en-->Publications<!--:--><!--:es-->Publicaciones<!--:-->'); ?></h2>
		

		<?php query_posts(array('post_type'=>'publicacoes','posts_per_page'=>-1)); 
		if ( have_posts() ) :
		    while ( have_posts() ) : the_post(); ?>
		<div class="row">
			<div class="col-xs-12 col-sm-3">
				<?php the_post_thumbnail( 'large' ); ?>
			</div>
			<div class="col-xs-12 col-sm-9">
				<h5><?php the_title(); ?></h5>
				<span><?php the_field('nome_do_autor'); ?></span>
				<?php 
					the_content();
          		?>
			</div>
		</div>
        <?php
		    endwhile;
		endif;
		wp_reset_query(); ?>
	</div>
</div>

<?php get_footer(); ?>