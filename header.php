<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till #main div
 *
 * @package Odin
 * @since 2.2.0
 */
?><!DOCTYPE html>
<html class="no-js" <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="<?php bloginfo('description'); ?>">
	<meta name="keywords" content="Lilian Gassen">
	<meta name="author" content="Lilian Gassen">
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php if ( ! get_option( 'site_icon' ) ) : ?>
		<link href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico" rel="shortcut icon" />
	<?php endif; ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/fonts/montserrat/stylesheet.css">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<!-- <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/main.css"> -->
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/estilo.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/libs/owl.carousel.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/libs/owl.theme.default.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/libs/owl.transitions.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/libs/lightgallery.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/assets/css/libs/font-awesome.css">

	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/jquery-3.1.1.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/owl.carousel.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/owl.carousel2.thumbs.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/lightgallery.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/all.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/libs/jquery.matchHeight-min.js"></script>
	
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/assets/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<a id="skippy" class="sr-only sr-only-focusable" href="#content">
		<div class="container">
			<span class="skiplink-text"><?php _e( 'Skip to content', 'odin' ); ?></span>
		</div>
	</a>

	<header>
		<?php if (is_page('Linguagem')) { ?>
		<div class="container menu-linguagem">
			<div class="col-sm-6 col-md-6 col-logo-topo">
				<a href="<?php echo site_url(); ?>" class="logo-topo"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png"></a>
			</div>
			<div class="col-sm-6 col-md-6 menu">
				<a href="<?php echo site_url(); ?>/pb/home">Português</a>
				<a href="<?php echo site_url(); ?>/en/home">English</a>
				<a href="<?php echo site_url(); ?>/es/home">Español</a>
			</div>
		</div>
		<?php } else { ?>
		<nav class="navbar navbar-default menu-home">
		  	<div class="container">
			    <div class="navbar-header">
			      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
			        <span class="sr-only">Toggle navigation</span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			        <span class="icon-bar"></span>
			      </button>
			      <a href="<?php echo site_url(); ?>"><img class="logo-topo" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png"></a>
			    </div>

			   	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			      	<ul class="nav navbar-nav">
			      		<!-- Dropdown Artes -->
				        <li class="dropdown">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php _e('<!--:pb-->Arte<!--:--><!--:en-->Art<!--:--><!--:es-->Arte<!--:-->'); ?></a>
				          <ul class="dropdown-menu">
				          	<!-- <div class="col-md-2 col-dropdown-menu"></div> -->
				          	<?php $category = get_queried_object(); ?>
							<?php $categorias = get_terms('artes', array("parent" => 0, "order" => ASC, "hide_empty" => false)); ?>
							<?php foreach ($categorias as $cat): ?>	 								
								<div class="col-md-2">
									<h5><?php echo $cat->name; ?></h5>
								<?php foreach (get_terms('artes', array("parent" => $cat->term_id, "hide_empty" => false)) as $sub): ?>
									<a data-fadeout  href="<?php echo get_term_link( $sub->slug, "artes" ); ?>">
										<?php echo $sub->name; ?>
									</a>
								<?php endforeach ?>
								</div>	
							<?php endforeach ?>
				          </ul>
				        </li>

				        <li><a href="<?php echo site_url(); ?>/processos"><?php _e('<!--:pb-->Processos<!--:--><!--:en-->Processes<!--:--><!--:es-->Procesos<!--:-->'); ?></a></li>
				        <li><a href="<?php echo site_url(); ?>/cronologia"><?php _e('<!--:pb-->Cronologia<!--:--><!--:en-->Chronology<!--:--><!--:es-->Cronología<!--:-->'); ?></a><li>
						<!-- Dropdown Exposições -->
				        <li class="dropdown">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php _e('<!--:pb-->Exposições<!--:--><!--:en-->Exhibitions<!--:--><!--:es-->Exposiciones<!--:-->'); ?></a>
				          <ul class="dropdown-menu dropdown-exposicoes">
				           <div class="exposicoes-options">
					           	<h5><?php _e('<!--:pb-->Exposições<!--:--><!--:en-->Exhibitions<!--:--><!--:es-->Exposiciones<!--:-->'); ?></h5>
					            <li><a href="<?php echo site_url(); ?>/exposicoes"><?php _e('<!--:pb-->Individuais<!--:--><!--:en-->Solo<!--:--><!--:es-->Individuales<!--:-->'); ?></a></li>
					            <li><a href="<?php echo site_url(); ?>/exposicoes_coletivas"><?php _e('<!--:pb-->Coletivas<!--:--><!--:en-->Group<!--:--><!--:es-->Colectivas<!--:-->'); ?></a></li>
					        </div>
				          </ul>
				        </li>

				        <li><a href="<?php echo site_url(); ?>/citacoes"><?php _e('<!--:pb-->Citações<!--:--><!--:en-->Quotes<!--:--><!--:es-->Citas<!--:-->'); ?></a><li>
						<!-- Dropdown Publicações -->
				        <li class="dropdown">
				          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Textos</a>
				          <ul class="dropdown-menu dropdown-menu-textos">
				          	<li><a href="<?php echo site_url(); ?>/textos_criticos"><?php _e('<!--:pb-->Textos Críticos<!--:--><!--:en-->Critical Texts<!--:--><!--:es-->Textos Críticos<!--:-->'); ?></a></li>
				          	<li><a href="<?php echo site_url(); ?>/textos_artistas"><?php _e('<!--:pb-->Textos de Artistas<!--:--><!--:en-->Artists Texts<!--:--><!--:es-->Textos de Artistas<!--:-->'); ?></a></li>
				            <li><a href="<?php echo site_url(); ?>/publicacoes"><?php _e('<!--:pb-->Publicações<!--:--><!--:en-->Publications<!--:--><!--:es-->Publicaciones<!--:-->'); ?></a></li>
				          </ul>
				        </li>
				        <li><a href="<?php echo site_url(); ?>/contato"><?php _e('<!--:pb-->Contato<!--:--><!--:en-->Contact<!--:--><!--:es-->Contacto<!--:-->'); ?></a><li>
			      	</ul>
			    </div><!-- /.navbar-collapse -->
		  	</div><!-- /.container-fluid -->
		</nav>
		<?php } ?>
	</header>
	<h1 style="color:transparent;position:absolute;">Lilian Gassen</h1>

