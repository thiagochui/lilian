<?php 

	/*$arte_metabox = new Odin_Metabox(
	    'arte', // Slug/ID do Metabox (obrigatório)
	    'Arte Configurações', // Nome do Metabox  (obrigatório)
	    'arte', // Slug do Post Type, sendo possível enviar apenas um valor ou um array com vários (opcional)
	    'normal', // Contexto (opções: normal, advanced, ou side) (opcional)
	    'high' // Prioridade (opções: high, core, default ou low) (opcional)
	);*/

	/*$arte_metabox->set_fields(
	    array(
	        array(
	            'id'          => 'test_text',
	            'label'       => __( 'Test Text', 'odin' ),
	            'type'        => 'text',
	            'description' => __( 'Descrição do campo de text', 'odin' )
	        )
	    )
	);*/



	/*$exposicoes_metabox = new Odin_Metabox(
	    'exposicoes', // Slug/ID do Metabox (obrigatório)
	    'Exposição Configurações', // Nome do Metabox  (obrigatório)
	    'exposicoes', // Slug do Post Type, sendo possível enviar apenas um valor ou um array com vários (opcional)
	    'normal', // Contexto (opções: normal, advanced, ou side) (opcional)
	    'high' // Prioridade (opções: high, core, default ou low) (opcional)
	);

	$exposicoes_metabox->set_fields(
	    array(
	        array(
	            'id'          => 'test_text',
	            'label'       => __( 'Test Text', 'odin' ),
	            'type'        => 'text',
	            'description' => __( 'Descrição do campo de text', 'odin' )
	        )
	    )
	);*/



	/*$publicacoes_metabox = new Odin_Metabox(
	    'publicacoes', // Slug/ID do Metabox (obrigatório)
	    'Publicação Configurações', // Nome do Metabox  (obrigatório)
	    'publicacoes', // Slug do Post Type, sendo possível enviar apenas um valor ou um array com vários (opcional)
	    'normal', // Contexto (opções: normal, advanced, ou side) (opcional)
	    'high' // Prioridade (opções: high, core, default ou low) (opcional)
	);

	$publicacoes_metabox->set_fields(
	    array(
	        array(
	            'id'          => 'autor_publicacao',
	            'label'       => __( 'Nome do Autor', 'odin' ),
	            'type'        => 'text',
	            'description' => __( 'Descrição do campo de text', 'odin' )
	        )
	    )
	);*/





	$citacoes_metabox = new Odin_Metabox(
	    'citacoes', // Slug/ID do Metabox (obrigatório)
	    'Citação Configurações', // Nome do Metabox  (obrigatório)
	    'citacoes', // Slug do Post Type, sendo possível enviar apenas um valor ou um array com vários (opcional)
	    'normal', // Contexto (opções: normal, advanced, ou side) (opcional)
	    'high' // Prioridade (opções: high, core, default ou low) (opcional)
	);

	$citacoes_metabox->set_fields(
	    array(
	        array(
	            'id'          => 'referencia_citacoes',
	            'label'       => __( 'Referência', 'odin' ),
	            'type'        => 'editor',
	            'description' => __( 'Referência da citação', 'odin' )
	        )
	    )
	);

?>