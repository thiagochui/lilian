<?php 

	$arte = new Odin_Post_Type(
	    'Arte', // Nome (Singular) do Post Type.
	    'arte' // Slug do Post Type.
	);

	$arte->set_labels(
	    array(
	        'menu_name' => __( 'Arte', 'odin' )
	    )
	);

	$arte->set_arguments(
	    array(
	        'supports' => array( 'title', 'editor', 'thumbnail' ),
	        'menu_icon' => 'dashicons-format-image'
	    )
	);




	$publicacoes = new Odin_Post_Type(
	    'Publicaçõe', // Nome (Singular) do Post Type.
	    'publicacoes' // Slug do Post Type.
	);

	$publicacoes->set_labels(
	    array(
	        'menu_name' => __( 'Publicações', 'odin' )
	    )
	);

	$publicacoes->set_arguments(
	    array(
	        'supports' => array( 'title', 'editor', 'thumbnail' ),
	        'menu_icon' => 'dashicons-book-alt'
	    )
	);




	$citacoes = new Odin_Post_Type(
	    'Citaçõe', // Nome (Singular) do Post Type.
	    'citacoes' // Slug do Post Type.
	);

	$citacoes->set_labels(
	    array(
	        'menu_name' => __( 'Citações', 'odin' )
	    )
	);

	$citacoes->set_arguments(
	    array(
	        'supports' => array( 'title', 'editor', 'thumbnail' ),
	        'menu_icon' => 'dashicons-format-aside'
	    )
	);



	$cronologia = new Odin_Post_Type(
	    'Cronologia', // Nome (Singular) do Post Type.
	    'cronologias' // Slug do Post Type.
	);

	$cronologia->set_labels(
	    array(
	        'menu_name' => __( 'Cronologia', 'odin' )
	    )
	);

	$cronologia->set_arguments(
	    array(
	        'supports' => array( 'title', 'editor', 'thumbnail' ),
	        'menu_icon' => 'dashicons-calendar-alt'
	    )
	);




	$processos = new Odin_Post_Type(
	    'Processos', // Nome (Singular) do Post Type.
	    'processos' // Slug do Post Type.
	);

	$processos->set_labels(
	    array(
	        'menu_name' => __( 'Processos', 'odin' )
	    )
	);

	$processos->set_arguments(
	    array(
	        'supports' => array( 'title', 'editor', 'thumbnail' ),
	        'menu_icon' => 'dashicons-portfolio'
	    )
	);



	$exposicoes_individuais = new Odin_Post_Type(
	    'Exposições Individuais', // Nome (Singular) do Post Type.
	    'exposicoes' // Slug do Post Type.
	);

	$exposicoes_individuais->set_labels(
	    array(
	        'menu_name' => __( 'Exposições Individuais', 'odin' )
	    )
	);

	$exposicoes_individuais->set_arguments(
	    array(
	        'supports' => array( 'title', 'editor', 'thumbnail' ),
	        'menu_icon' => 'dashicons-format-gallery'
	    )
	);



	$exposicoes_coletivas = new Odin_Post_Type(
	    'Exposições Coletivas', // Nome (Singular) do Post Type.
	    'exposicoes_coletivas' // Slug do Post Type.
	);

	$exposicoes_coletivas->set_labels(
	    array(
	        'menu_name' => __( 'Exposições Coletivas', 'odin' )
	    )
	);

	$exposicoes_coletivas->set_arguments(
	    array(
	        'supports' => array( 'title', 'editor', 'thumbnail' ),
	        'menu_icon' => 'dashicons-images-alt'
	    )
	);





	$textos_criticos = new Odin_Post_Type(
	    'Textos Crítico', // Nome (Singular) do Post Type.
	    'textos_criticos' // Slug do Post Type.
	);

	$textos_criticos->set_labels(
	    array(
	        'menu_name' => __( 'Textos Críticos', 'odin' )
	    )
	);

	$textos_criticos->set_arguments(
	    array(
	        'supports' => array( 'title', 'editor' ),
	        'menu_icon' => 'dashicons-format-quote'
	    )
	);




	$textos_artistas = new Odin_Post_Type(
	    'Textos de Artista', // Nome (Singular) do Post Type.
	    'textos_artistas' // Slug do Post Type.
	);

	$textos_artistas->set_labels(
	    array(
	        'menu_name' => __( 'Textos de Artistas', 'odin' )
	    )
	);

	$textos_artistas->set_arguments(
	    array(
	        'supports' => array( 'title', 'editor' ),
	        'menu_icon' => 'dashicons-format-quote'
	    )
	);




	$slider_home = new Odin_Post_Type(
	    'Slider da Home', // Nome (Singular) do Post Type.
	    'slider_home' // Slug do Post Type.
	);

	$slider_home->set_labels(
	    array(
	        'menu_name' => __( 'Slider da Home', 'odin' )
	    )
	);

	$slider_home->set_arguments(
	    array(
	        'supports' => array( 'title', 'editor', 'thumbnail' ),
	        'menu_icon' => 'dashicons-images-alt2'
	    )
	);

?>