<?php 

    function odin_arte_taxonomy() {
        $arte = new Odin_Taxonomy(
            'Categoria', // Nome (Singular) da nova Taxonomia.
            'artes', // Slug do Taxonomia.
            'arte' // Nome do tipo de conteúdo que a taxonomia irá fazer parte.
        );

        $arte->set_labels(
            array(
                'menu_name' => __( 'Categorias', 'odin' )
            )
        );

        $arte->set_arguments(
            array(
                'hierarchical' => true
            )
        );
    }

add_action( 'init', 'odin_arte_taxonomy', 1 );





function odin_exposicoes_taxonomy() {
        $exposicao = new Odin_Taxonomy(
            'Categoria', // Nome (Singular) da nova Taxonomia.
            'cat_exposicoes', // Slug do Taxonomia.
            'exposicoes' // Nome do tipo de conteúdo que a taxonomia irá fazer parte.
        );

        $exposicao->set_labels(
            array(
                'menu_name' => __( 'Categorias', 'odin' )
            )
        );

        $exposicao->set_arguments(
            array(
                'hierarchical' => true
            )
        );
    }

add_action( 'init', 'odin_exposicoes_taxonomy', 1 );


function odin_citacoes_taxonomy() {
        $citacoes = new Odin_Taxonomy(
            'Categoria', // Nome (Singular) da nova Taxonomia.
            'cat_citacoes', // Slug do Taxonomia.
            'citacoes' // Nome do tipo de conteúdo que a taxonomia irá fazer parte.
        );

        $citacoes->set_labels(
            array(
                'menu_name' => __( 'Categorias', 'odin' )
            )
        );

        $citacoes->set_arguments(
            array(
                'hierarchical' => true
            )
        );
    }

add_action( 'init', 'odin_citacoes_taxonomy', 1 );



function odin_cronologia_taxonomy() {
        $citacoes = new Odin_Taxonomy(
            'Categoria', // Nome (Singular) da nova Taxonomia.
            'cat_cronologia', // Slug do Taxonomia.
            'cronologias' // Nome do tipo de conteúdo que a taxonomia irá fazer parte.
        );

        $citacoes->set_labels(
            array(
                'menu_name' => __( 'Categorias', 'odin' )
            )
        );

        $citacoes->set_arguments(
            array(
                'hierarchical' => true
            )
        );
    }

add_action( 'init', 'odin_cronologia_taxonomy', 1 );




function odin_processos_taxonomy() {
        $processos = new Odin_Taxonomy(
            'Categoria', // Nome (Singular) da nova Taxonomia.
            'cat_processos', // Slug do Taxonomia.
            'processos' // Nome do tipo de conteúdo que a taxonomia irá fazer parte.
        );

        $processos->set_labels(
            array(
                'menu_name' => __( 'Categorias', 'odin' )
            )
        );

        $processos->set_arguments(
            array(
                'hierarchical' => true
            )
        );
    }

add_action( 'init', 'odin_processos_taxonomy', 1 );

?>