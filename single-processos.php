<?php get_header(); ?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div class="processos content-size">
			<h5><?php the_title(); ?></h5>
			<span><a href="<?php echo site_url(); ?>/processos"><?php _e('<!--:pb-->Processos<!--:--><!--:en-->Processes<!--:--><!--:es-->Procesos<!--:-->'); ?></a></span>
		<div class="conteudo-processos">

			<?php 
				$slider = get_field('imagens_slider_processos');
			?>
			<div class="owl-carousel owl-theme" id="slider-single-processos" data-slider-id="1">
				<?php foreach($slider as $foto): ?>
				<?php $img =  $foto['imagem_processo'];?>
				<div class="item">
					<div class="col-xs-12 col-sm-12 col-md-12 col-image">
						<img src="<?php echo $img['sizes']['large']; ?>">
					</div>
					<div class="col-xs-12 col-sm-6 col-md-12 col-conteudo-processos hidden-xs hidden-sm">
						<?php echo $foto['texto_imagem_processo']; ?>
						<?php if($foto['fotografo_processo']) { ?>
							<span class="nome-fotografo-processos fotografo-slider">© <?php echo $foto['fotografo_processo']; ?></span>
						<?php } ?>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-12 hidden-md hidden-lg col-texto">
						<?php echo $foto['texto_imagem_processo']; ?>
						<?php if($foto['fotografo_processo']) { ?>
							<span class="nome-fotografo-processos fotografo-slider">© <?php echo $foto['fotografo_processo']; ?></span>
						<?php } ?>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4"></div>
				</div>
				<?php endforeach; ?>
			</div>
			<div class="video row">
				<?php 
				$videos = get_field('conteudo_processos');
				foreach ($videos as $video):
				?>
				<div class="col-xs-12 col-sm-12 col-md-6 col-iframe">
					<?php 
						if ($video['video']) {   
							echo $video['video']; 
					 	} 
					 	if ($video['descricao']) {   
							echo $video['descricao']; 
					 	}
					?>
				</div>
			<?php endforeach; ?>
			</div>

		</div>
	</div>

<?php endwhile; endif; ?>

<?php
get_footer(); ?>