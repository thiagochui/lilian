<?php get_header(); ?>

<div class="single-textos">
		<div class="container">
			<h2><?php the_title(); ?></h2>
			<div class="col-xs-12 col-sm-12">
				<div class="box-textos col-xs-12 col-sm-8">
					<h4><?php echo get_field('autor_texto_artista'); ?></h4>

					<p><?php the_content(); ?></p>

					<?php if(get_field('notas_texto_artista')) { ?>
					<div class="box-notas">
						<h5><?php _e('<!--:pb-->NOTAS<!--:--><!--:en-->GRADES<!--:--><!--:es-->NOTAS<!--:-->'); ?></h5>
						<?php echo get_field('notas_texto_artista'); ?>
					</div>
					<?php } ?>
					<?php if(get_field('referencias_texto_artista')) { ?>
					<div class="box-referencias">
						<h5><?php _e('<!--:pb-->REFERÊNCIAS<!--:--><!--:en-->REFERENCES<!--:--><!--:es-->REFERENCIAS<!--:-->'); ?></h5>
						<?php echo get_field('referencias_texto_artista'); ?>
					</div>
					<?php } ?>
				</div>
			</div>


		</div>
	</div>


<?php get_footer(); ?>