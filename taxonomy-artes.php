<?php

get_header();

?>

<div id="artes">
	<div class="content-size">
		<h2>
			<?php
				echo print_taxonomic_ranks( get_the_terms( $post->ID, 'artes' ) );
			?>
		</h2>

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="col-xs-12 col-sm-6 col-md-4 conteudo-archive-arte">
				<?php $img = get_field('imagem_destaque_arte'); ?>
				<a href="<?php the_permalink(); ?>"><img src="<?php echo $img['sizes']['large']; ?>"></a>
				<h5><?php the_title(); ?></h5>
				<p><?php the_field('conteudo_arte_archive'); ?></p>
			</div>
		<?php endwhile; endif; ?>
	</div>
</div>


<?php
get_footer(); ?>