jQuery(document).ready(function($) {

	$("#slider-linguagem").owlCarousel({
		loop : true,
		nav : false,
		items : 1,
	    autoPlay : true,
	    pagination : false,
	    animateOut : 'fadeOut',
	    autoplay : true,
	    dots: false
	});

	$("#slider-home").owlCarousel({
		loop : true,
		nav : false,
		items : 1,
	    autoPlay : true,
	    pagination : false,
	    animateOut : 'fadeOut',
	    autoplay : true,
	    dots: false
	});

	
	$("#slider-single-artes, #slider-single-individuais, #slider-single-processos").owlCarousel({
		loop : true,
	    margin : 0,
	    nav : true,
	    items : 1,
	    navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
	    dots : false,
	    thumbs : true,
    	thumbsPrerendered : true,
    	animateOut : 'fadeOut'
	});

	$("#slider-modal-artes, #slider-modal-individuais").owlCarousel({
		loop : true,
	    margin : 0,
	    nav : true,
	    items : 1,
	    navText : ['<i class="fa fa-angle-left" aria-hidden="true"></i>', '<i class="fa fa-angle-right" aria-hidden="true"></i>'],
	    dots : false,
	    thumbs : true,
    	thumbsPrerendered : true,
    	animateOut : 'fadeOut'
	});

	$(function() {
		$('#artes .conteudo-archive-arte').matchHeight();
	});
	$(function() {
		$('.coluna-artes').matchHeight();
	});

	$(".single-artes h4").click(function(){
		window.history.back();
	});

	

});