<?php get_header(); ?>
	
	<div class="archive-textos">
		<div class="container">
			<h2>Textos Críticos</h2>
			
			<div class="col-xs-12 col-sm-12">
				<?php query_posts(array('post_type'=>'textos_criticos','posts_per_page'=>-1)); 
				if ( have_posts() ) :
				    while ( have_posts() ) : the_post(); ?>
				<div class="box-textos">
					<h5><?php the_title(); ?></h5>
					<p><?php echo get_field('autor_texto_critico'); ?> - <?php echo get_field('ano_texto_critico'); ?></p>
					<p><?php echo get_field('informacoes_complementares_critico'); ?></p>
					<a href="<?php the_permalink(); ?>">[<?php _e('<!--:pb-->MAIS<!--:--><!--:en-->MORE<!--:--><!--:es-->MÁS<!--:-->'); ?>]</a>
				</div>
				<?php
				    endwhile;
				endif;
				wp_reset_query(); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>