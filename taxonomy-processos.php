<?php

get_header();

$tax = get_term_by( "name", get_query_var("term"), "processos");

if ($tax->parent) {
	$parent = get_term($tax->parent, "processos" );
}

?>

<div id="processos">
	<div class="content-size">
		<h2>
			<?php
				if ($parent) {
					echo $parent->name . " / " . $tax->name;
				} else {
					echo $tax->name;
				}
			?>
		</h2>

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<?php $img = get_field('imagem_destaque_processo'); ?>
				<a href="<?php the_permalink(); ?>"><img src="<?php echo $img['sizes']['large']; ?>"></a>
				<h5><?php the_title(); ?></h5>
				<p><?php the_field('conteudo_processo'); ?></p>
			</div>
		<?php endwhile; endif; ?>
	</div>
</div>


<?php
get_footer(); ?>