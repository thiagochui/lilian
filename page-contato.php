<?php get_header(); ?>
	
	<div class="contato">
		<div class="content-size">
			<h2><?php the_title(); ?></h2>

			<?php if(qtrans_getLanguage()==='pb'): ?>
			<?php echo do_shortcode( '[contact-form-7 id="37" title="Formulário de contato 1"]' ); ?>
			<?php endif; ?>

			<?php if(qtrans_getLanguage()==='en'): ?>
			<?php echo do_shortcode( '[contact-form-7 id="1025" title="Formulário Inglês"]' ); ?>
			<?php endif; ?>
			
			<?php if(qtrans_getLanguage()==='es'): ?>
			<?php echo do_shortcode( '[contact-form-7 id="1026" title="Formulário Espanhol"]' ); ?>
			<?php endif; ?>

		</div>

	</div>
	
<?php get_footer(); ?>