<?php get_header(); ?>

<?php $category = get_queried_object(); ?>

<div id="archive-exposicoes-individuais">
	<div class="content-size">
		<h2><?php _e('<!--:pb-->EXPOSIÇÕES INDIVIDUAIS<!--:--><!--:en-->SOLO EXHIBITIONS<!--:--><!--:es-->EXPOSICIONES INDIVIDUALES<!--:-->'); ?></h2>
		

		<?php query_posts(array('post_type'=>'exposicoes','posts_per_page'=>-1)); 
		if ( have_posts() ) :
		    while ( have_posts() ) : the_post(); ?>
		<div class="row">
			<div class="col-xs-12 col-sm-3">
				<?php $img = get_field('imagem_principal_individuais'); ?>
				<a href="<?php the_permalink(); ?>"><img src="<?php echo $img['sizes']['large']; ?>"></a>
			</div>
			<div class="col-xs-12 col-sm-9">
				<h5><?php the_title(); ?></h5>
				<span><?php the_field('sub_titulo_individuais'); ?></span>
				<p>
				<?php 
					$summary = get_field('conteudo_exposicoes_individuais');
          			echo substr($summary, 0, 200).'...'; 
          		?>
          		</p>
				<a class="btn-ver-mais" href="<?php the_permalink(); ?>">[<?php _e('<!--:pb-->VER MAIS<!--:--><!--:en-->VIEW MORE<!--:--><!--:es-->VER MÁS<!--:-->'); ?>]</a>
			</div>
		</div>
        <?php
		    endwhile;
		endif;
		wp_reset_query(); ?>
	</div>
</div>


<?php
get_footer(); ?>