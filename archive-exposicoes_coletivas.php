<?php get_header(); ?>

<div id="exposicoes-coletivas">
	<div class="content-size">
		<h2><?php _e('<!--:pb-->EXPOSIÇÕES COLETIVAS<!--:--><!--:en-->GROUP EXHIBITIONS<!--:--><!--:es-->EXPOSICIONES COLECTIVAS<!--:-->'); ?></h2>

        <?php query_posts(array('post_type'=>'exposicoes_coletivas','order'=>'DESC','posts_per_page' => -1));

		if ( have_posts() ) :
		    while ( have_posts() ) : the_post(); ?>

		        <div class="col-xs-12 col-sm-12 col-coletivas">
				<h5><?php echo the_title(); ?></h5>
				<p><?php echo the_field('data_local'); ?></p>
				<p><?php echo the_field('curadoria'); ?></p>
			</div>

		        <?php
		    endwhile;
		endif;
		wp_reset_query(); ?>
	</div>
</div>


<?php
get_footer(); ?>