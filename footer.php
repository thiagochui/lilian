<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main div element.
 *
 * @package Odin
 * @since 2.2.0
 */
?>
	<footer id="footer" role="contentinfo">
		<div class="container">
			<p>Lilian Gassen <?php echo date( 'Y' ); ?> &copy; Todos os direitos reservados</p>
		</div><!-- .container -->
	</footer><!-- #footer -->

	<?php wp_footer(); ?>
</body>
</html>
