<?php get_header(); ?>

	<div class="container">
		<?php query_posts(array('post_type'=>'slider_home','posts_per_page'=>-1)); ?>
			<div class="owl-carousel owl-theme" id="slider-home">
				<?php if ( have_posts() ) :
				    while ( have_posts() ) : the_post(); ?>
				<?php $imagem = get_field('imagem_slider_home'); ?>
					<div class="item">
						<img src="<?php echo $imagem['sizes']['large']; ?>">
					</div>
			 	<?php
			    	endwhile;
				endif; ?>
			</div>
		<?php wp_reset_query(); ?>
	</div>

<?php get_footer(); ?>