<?php get_header(); ?>

	<div id="cronologia">
		<div class="container">
			<h2>Cronologia</h2>

			<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-2">
				<div class="box-citacoes box-destaque">
				<?php
					$args = array(
						'posts_per_page'   => 3,
						'orderby'          => 'date',
						'order'            => 'ASC',
						'post_type'        => 'cronologias',
						'tax_query' => array(
							array(
								'taxonomy' => 'cat_cronologia',
								'field'    => 'slug',
								'terms'    => 'destaque',
							)
						)
					);
					$cronologias = get_posts( $args );
					foreach($cronologias as $cronologia) { ?>
						<h5><?php echo $cronologia->post_title; ?></h5>
						<p><?php echo $cronologia->post_content; ?></p>
					<?php }  ?>
				</div>
			</div>
			
			
			<?php $imagem = get_field('imagem_principal_cronologia', 65); ?>
			<img class="img-lilian" src="<?php echo $imagem['sizes']['large']; ?>"> 
			
			<div class="col-xs-12 col-sm-12 col-md-10 col-md-offset-2">
			<?php
				$args = array(
					'posts_per_page'   => -1,
					'orderby'          => 'date',
					'order'            => 'ASC',
					'post_type'        => 'cronologias',
					'tax_query' => array(
						array(
							'taxonomy' => 'cat_cronologia',
							'field'    => 'slug',
							'terms'    => 'normal',
						)
					)
				);
				$cronologias = get_posts( $args );
				foreach($cronologias as $cronologia) { ?>
				<div class="box-citacoes">
					<h5><?php echo $cronologia->post_title; ?></h5>
					<p><?php echo $cronologia->post_content; ?></p>
				</div>
				<?php }  ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>