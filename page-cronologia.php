<?php get_header(); ?>

	<div id="cronologia">
		<div class="container">
			<h2><?php _e('<!--:pb-->Cronologia<!--:--><!--:en-->Chronology<!--:--><!--:es-->Cronología<!--:-->'); ?></h2>
			
			<div class="row row-cronologia">
					<?php query_posts(array('post_type'=>'cronologias','posts_per_page'=>-1, 'orderby'=>'date','order'=>'ASC')); 
					if ( have_posts() ) :
					    while ( have_posts() ) : the_post(); ?>
					<div class="col-xs-12 col-sm-12">
						<div class="box-citacoes col-xs-12 col-sm-6">
							<h5><?php echo the_title(); ?></h5>
							<p><?php echo the_content(); ?></p>
						</div>
						<div class="box-image-cronologia col-xs-12 col-sm-6">
							<?php 
							$imagem = get_field('imagem_da_cronologia');
							if($imagem) {
							?>
							<img class="img-lilian img-lilian-1" src="<?php echo $imagem['sizes']['large']; ?>">
							<span class="legenda-cronologia"><?php echo get_field('legenda_imagem_cronologia'); ?></span>
							<?php } ?>
						</div>
					</div>
					<?php
					    endwhile;
					endif;
					wp_reset_query(); ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>