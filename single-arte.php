<?php get_header(); ?>

<?php
		$args = array(
			'posts_per_page'   => 9999,
			'post_type'        => 'arte',
			'post_status'      => 'publish',
			'offset'		   => 0,
		);
		$artes = get_posts( $args ); ?>

	<div class="single-artes content-size">
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4">
				<h5><?php the_title(); ?></h5>
				<h4><?php echo print_taxonomic_ranks( get_the_terms( $post->ID, 'artes' ) ); ?></h4>
			</div>
			<span class="open-gallery" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-artes">
				<img src="<?php echo get_template_directory_uri(); ?>/assets/images/open-gallery.png">
			</span>
		</div>
		<div class="imagens-galeria">
			<?php 
				$slider = get_field('imagens_slider_arte');
			?>

			<div class="owl-carousel owl-theme" id="slider-single-artes" data-slider-id="1">
				<?php foreach($slider as $foto): ?>
				<?php $img =  $foto['imagem_arte'];?>
				<div class="item">
					<div class="col-xs-12 col-sm-6 col-md-4 hidden-xs hidden-sm texto-imagem-arte coluna-artes">
						<?php echo $foto['texto_imagem_arte']; ?>
						<?php if($foto['fotografo_artes']) { ?>
							<span class="nome-fotografo fotografo-slider">© <?php echo $foto['fotografo_artes']; ?></span>
						<?php } ?>
						<?php //do_action('back_button'); ?>
						<button id="my-back-button" class="btn button my-back-button" onclick="javascript:history.back()">voltar</button>
					</div>
					<div class="col-xs-12 col-sm-12 col-md-8 img-arte coluna-artes">
						<img src="<?php echo $img['sizes']['large']; ?>">
					</div>
					<div class="col-xs-12 col-sm-12 col-md-4 hidden-md hidden-lg">
						<?php echo $foto['texto_imagem_arte']; ?>
						<?php if($foto['fotografo_artes']) { ?>
							<span class="nome-fotografo fotografo-slider">© <?php echo $foto['fotografo_artes']; ?></span>
						<?php } ?>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-4"></div>
				</div>
				<?php endforeach; ?>
			</div>

			<?php 
			$link_processo = get_field('link_processos_trabalho');
			if($link_processo) { ?>
				<a href="<?php echo the_field('link_processos_trabalho'); ?>" class="link-processos-single-artes"><?php _e('<!--:pb-->Veja o processo<!--:--><!--:en-->See the process<!--:--><!--:es-->Ver el proceso<!--:-->'); ?></a>
			<?php } ?>
			
			<div class="artes-galeria">
			</div>
			<div class="modal fade" id="modal-artes" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content content-size">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/close.png">
							</button>
							<h1 class="modal-title"><?php the_title(); ?></h1>
						</div>
						<div class="modal-body">
							<?php $galeria = get_field('imagens_slider_arte');  ?>
							<div class="owl-carousel owl-theme" id="slider-modal-artes">
								<?php foreach($galeria as $foto): ?>
								<?php $imagem =  $foto['imagem_arte'];?>
								<div class="item">
									<img src="<?php echo $imagem['sizes']['large']; ?>">
									<?php if($foto['fotografo_artes']) { ?>
									<span class="nome-fotografo">© <?php echo $foto['fotografo_artes']; ?></span>
									<?php } ?>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->
			<div class="col-xs-12 col-sm-6 col-md-4"></div>
			<div class="col-xs-12 col-sm-12 col-md-8" style="padding:0px!important;">
				<div class="galeria-single hidden-xs">
					<div class="owl-thumbs" data-slider-id="1">
						<?php $galeria = get_field('galeria_thumbs'); 
						foreach($galeria as $foto): ?>
							<div class="col-xs-12 col-sm-4 col-md-3 col-img-galeria owl-thumb-item">
								<img src="<?php echo $foto['sizes']['large']; ?>">
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php
get_footer(); ?>