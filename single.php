<?php
/**
 * @package Odin
 * @since 2.2.0
 */

get_header(); ?>

<?php $category = get_queried_object(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div id="artes">
	<div class="container">
		<?php $categorias = get_terms('cat_arte', array("parent" => 0, "hide_empty" => false)); ?>
		<?php foreach ($categorias as $cat): ?>
		<h2>
			<?php echo $cat->name; ?> /
			<?php foreach (get_terms('cat_arte', array("parent" => $cat->term_id, "hide_empty" => false)) as $sub): ?>
				<?php echo $sub->name; ?>
		</h2>
			<?php endforeach ?>	
		<?php endforeach ?>

		<section class="test">
			<?php
			$args = array(
				'posts_per_page'   => 9999,
				'post_type'        => 'arte',
				'post_status'      => 'publish'
			);
			$artes = get_posts( $args );

			foreach($artes as $arte):
				$galeria = get_field('galeria_imagens_arte', $arte->ID);
				?>
				<div class="col-xs-12 col-sm-6 col-md-4">
					<?php $img = get_field('imagem_destaque_arte'); ?>
					<a id="image-open-<?php echo $arte->ID; ?>"><img src="<?php echo $img['sizes']['large']; ?>"></a>
					<h5><?php echo $arte->post_title; ?></h5>
					<p><?php echo $arte->post_content; ?></p>
				</div>

				<?php
					$final = array();
					if ($galeria) {
						foreach($galeria as $gallery):
							$final[] = array(
								"src" => $gallery['sizes']['large'],
								"thumb" => $gallery['sizes']['thumbnail']
							);
						endforeach;
					}
					?>
					<script type="text/javascript">
						$('#image-open-<?php echo $arte->ID; ?>').on('click', function() {
							$(this).lightGallery({
								dynamic: true,
								dynamicEl: <?php echo json_encode($final); ?>
							});
						});
					</script>
	            <?php
			endforeach;
		?>
		</section>
	</div>
</div>

<?php endwhile; endif; ?>

<?php
get_footer(); ?>
