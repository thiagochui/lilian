<?php get_header(); ?>

<?php
		$args = array(
			'posts_per_page'   => 9999,
			'post_type'        => 'exposicoes',
			'post_status'      => 'publish',
			'offset'		   => 0,
		);
		$exposicoes = get_posts( $args ); ?>

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div class="single-individuais content-size">
		<div class="col-xs-12 col-sm-6 col-md-4">
			<h2><?php the_title(); ?></h2>
			<a href="<?php echo site_url(); ?>/exposicoes"><span><?php _e('<!--:pb-->Exposições Individuais<!--:--><!--:en-->Exhibitions Solo<!--:--><!--:es-->Exposiciones Individuales<!--:-->'); ?></span></a>
		</div>
		<div class="col-xs-12 col-sm-12 paragrafo-single-exposicoes">
			<p><?php echo the_field('conteudo_exposicoes_individuais'); ?></p>
		</div>
		<div class="imagens-galeria col-xs-12 col-sm-12">
			<?php $galeria = get_field('imagens_exposicoes_individuais');  ?>
			<div class="owl-carousel owl-theme" id="slider-single-individuais" data-slider-id="2">
				<?php foreach($galeria as $foto): ?>
				<?php $img = $foto['imagem_individuais']; ?>
				<div class="item">
					<img src="<?php echo $img['sizes']['large']; ?>">
					<?php if($foto['fotografo_individuais']) { ?>
					<span class="nome-fotografo slider-fotografo-individuais">© <?php echo $foto['fotografo_individuais']; ?></span>
					<?php } ?>
				</div>
				<?php endforeach; ?>
			</div>
			
			<div class="artes-galeria">
				<span class="open-gallery" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#modal-artes">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/open-gallery.png">
				</span>
			</div>
			<div class="modal fade" id="modal-artes" tabindex="-1" role="dialog">
				<div class="modal-dialog" role="document">
					<div class="modal-content content-size">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/close.png">
							</button>
							<h1 class="modal-title"><?php the_title(); ?></h1>
						</div>
						<div class="modal-body">
							<?php $galeria = get_field('imagens_exposicoes_individuais');  ?>
							<div class="owl-carousel owl-theme" id="slider-modal-individuais">
								<?php foreach($galeria as $foto): ?>
								<?php $image = $foto['imagem_individuais']; ?>
								<div class="item">
									<img src="<?php echo $image['sizes']['large']; ?>">
									<?php if($foto['fotografo_individuais']) { ?>
									<span class="nome-fotografo">© <?php echo $foto['fotografo_individuais']; ?></span>
									<?php } ?>
								</div>
								<?php endforeach; ?>
							</div>
						</div>
					</div><!-- /.modal-content -->
				</div><!-- /.modal-dialog -->
			</div><!-- /.modal -->

			<div class="galeria-single">
				<div class="owl-thumbs" data-slider-id="2">
					<?php $galeria = get_field('galeria_thumbs_individuais'); 
					foreach($galeria as $foto): ?>
						<div class="col-xs-12 col-sm-6 col-md-3 col-img-galeria owl-thumb-item">
							<img src="<?php echo $foto['sizes']['large']; ?>">
						</div>
					<?php endforeach; ?>
				</div>
			</div>
		</div>
	</div>

	<?php endwhile; endif; ?>

<?php
get_footer(); ?>