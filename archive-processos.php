<?php get_header(); ?>

<?php $category = get_queried_object(); ?>

<div id="processos">
	<div class="content-size">
		<h2><?php _e('<!--:pb-->PROCESSOS<!--:--><!--:en-->PROCESSES<!--:--><!--:es-->PROCESOS<!--:-->'); ?></h2>

		<?php query_posts(array('post_type'=>'processos','posts_per_page'=>-1)); 
		if ( have_posts() ) :
		    while ( have_posts() ) : the_post(); ?>
			<div class="col-xs-12 col-sm-6 col-md-4 col-archive-processos">
				<?php $img = get_field('imagem_destaque_processos'); ?>
				<a href="<?php the_permalink(); ?>"><img src="<?php echo $img['sizes']['large']; ?>"></a>
				<h5><?php the_title(); ?></h5>
			</div>
			 <?php
			    endwhile;
			endif;
			wp_reset_query(); ?>
	</div>
</div>


<?php
get_footer(); ?>